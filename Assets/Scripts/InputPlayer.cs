﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputPlayer : MonoBehaviour
{
    public bool activeObject;
    public int indexI;
    public int indexJ;
    private void Start() 
    {
        activeObject = true;
    }

    private void OnMouseDown() 
    {
        if(activeObject)
        {
            if(GameManager.instance.playerTurn && GameManager.instance.currentGameState == GameManager.GameStates.Gameplay)
            {
                gameObject.transform.GetChild(0).GetComponent<TextMesh>().text = GameManager.instance.human.ToString();
                GameManager.instance.playerTurn = false;
                GameManager.instance.currentCell = gameObject;
                activeObject = false;
                UIRef.instance.PutDataInTable();
                UIRef.instance.RefreshUI();
                if(GameManager.instance.EndGameConditionPlayer())
                {
                    UIRef.instance.PlayerWins();
                }
            }
        }
    }
}