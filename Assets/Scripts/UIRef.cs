﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRef : MonoBehaviour
{
    public static UIRef instance;
    public Text turnState;
    public GameObject startPanel;
    public GameObject finalPanel;
    public Text finalText;
    void Start()
    {
        if(!instance)
        {
            instance = this;
        }
    }

    public void RefreshUI()
    {
        if(GameManager.instance.playerTurn)
        {
            turnState.text = "Your Turn";
            turnState.color = Color.green;
        }
        else
        {
            turnState.text = "AI Turn";
            turnState.color = Color.red;
        }
    }

    public void PutDataInTable()
    {
        int i = 0;
        int j = 0;
        foreach (var item in GameManager.instance.guiTable)
        {
            if(item.transform.GetChild(0).GetComponent<TextMesh>().text != "")
            {
                GameManager.instance.table[i, j] = item.transform.GetChild(0).GetComponent<TextMesh>().text[0];
            }
            if(j == GameManager.instance.n - 1)
            {
                j = 0;
                i++;
            }
            else
            {
                j++;
            }
        }
    }

    public void PutTableInGUI(int row, int column)
    {
        int i = 0;
        int j = 0;
        foreach (var item in GameManager.instance.guiTable)
        {
            if(i == row && j == column)
            {
                item.transform.GetChild(0).GetComponent<TextMesh>().text = GameManager.instance.table[i, j].ToString();
                item.transform.GetComponent<InputPlayer>().activeObject = false;
            }
            if(j == GameManager.instance.n - 1)
            {
                j = 0;
                i++;
            }
            else
            {
                j++;
            }
        }
    }

    public void PlayerWins()
    {
        finalText.text = "Humanity Wins!";
        finalText.color = Color.green;
        finalPanel.SetActive(true);
        GameManager.instance.currentGameState = GameManager.GameStates.EndMenu;
    }

    public void AIWins()
    {
        finalText.text = "AI Wins!";
        finalText.color = Color.red;
        finalPanel.SetActive(true);
        GameManager.instance.currentGameState = GameManager.GameStates.EndMenu;
    }

    public void Equall()
    {
        finalText.text = "Humanity still fighting!!!";
        finalText.color = Color.grey;
        finalPanel.SetActive(true);
        GameManager.instance.currentGameState = GameManager.GameStates.EndMenu;
    }
}