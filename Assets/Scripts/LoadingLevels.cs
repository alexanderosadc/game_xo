﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingLevels : MonoBehaviour {

	public void ResetSettings()
    {
        for(int i = 0; i < GameManager.instance.n; i++)
        {
            for(int j = 0; j < GameManager.instance.n; j++)
            {
                GameManager.instance.table[i, j] = '1';
            }
        }

        foreach(var item in GameManager.instance.guiTable)
        {
            item.GetComponent<InputPlayer>().activeObject = true;
            item.transform.GetChild(0).GetComponent<TextMesh>().text = "";
        }
		GameManager.instance.currentGameState = GameManager.GameStates.StartMenu;
		UIRef.instance.startPanel.SetActive(true);
		UIRef.instance.finalPanel.SetActive(false);
    }

	public void PlayAsX()
	{
		GameManager.instance.human = 'x';
		GameManager.instance.aiBot = '0';
		GameManager.instance.playerTurn = false;
	}
	public void PlayAs0()
	{
		GameManager.instance.human = '0';
		GameManager.instance.aiBot = 'x';
		GameManager.instance.playerTurn = true;
	}

	public void StartGame()
	{
		UIRef.instance.RefreshUI();
		GameManager.instance.currentGameState = GameManager.GameStates.Gameplay;
	}

}
