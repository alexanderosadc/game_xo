﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiBot : MonoBehaviour {
	
	bool iterator = false;
	private void Start() 
	{
		StartCoroutine(Delayed(.5f));
	}
	IEnumerator Delayed(float time)
	{
		while(true)
		{
			if(iterator)
			{
				yield return new WaitForSeconds(time);
				findBestMove();
			}
			else
			{
				yield return new WaitForSeconds(1f);
			}
		}
	}
	private void Update() {
		if(!GameManager.instance.playerTurn && GameManager.instance.currentGameState == GameManager.GameStates.Gameplay)
		{
			Debug.Log("AAAA");
			// findBestMove();
			iterator = true;
			
		}
	}
	bool isMovesLeft(char[,] table) 
	{ 
		for (int i = 0; i<3; i++) 
			for (int j = 0; j<3; j++) 
				if (table[i, j]=='1') 
					return true; 
		return false; 
	} 
	int Evaluate(char[,] table)
	{
		for (int row = 0; row<3; row++) 
		{ 
			if (table[row, 0]==table[row, 1] && 
				table[row, 1]==table[row, 2]) 
			{ 
				if (table[row, 0]==GameManager.instance.aiBot) 
					return +10; 
				else if (table[row, 0]==GameManager.instance.human) 
					return -10; 
			} 
		} 

		for (int col = 0; col<3; col++) 
		{ 
			if (table[0, col]==table[1, col] && 
				table[1, col]==table[2, col]) 
			{ 
				if (table[0, col]==GameManager.instance.aiBot) 
					return +10; 
	
				else if (table[0, col]==GameManager.instance.human) 
					return -10; 
			} 
		} 
	

		if (table[0, 0]==table[1, 1] && table[1, 1]==table[2, 2]) 
		{ 
			if (table[0, 0]==GameManager.instance.aiBot) 
				return +10; 
			else if (table[0, 0]==GameManager.instance.human) 
				return -10; 
		} 
	
		if (table[0, 2]==table[1, 1] && table[1, 1]==table[2, 0]) 
		{ 
			if (table[0, 2]==GameManager.instance.aiBot) 
				return +10; 
			else if (table[0, 2]==GameManager.instance.human) 
				return -10; 
		} 
	
		return 0; 
	}

	int MiniMax(char[,] table, int depth, bool isMax)
	{
		int score = Evaluate(table); 
  
		if (score == 10) 
			return score; 
	
		if (score == -10) 
			return score; 
	
		if (isMovesLeft(table)==false) 
			return 0; 
	
		if (isMax) 
		{ 
			int best = -1000; 
	
			for (int i = 0; i<3; i++) 
			{ 
				for (int j = 0; j<3; j++) 
				{ 
					if (table[i, j]=='1') 
					{ 
						table[i, j] = GameManager.instance.aiBot; 
						best = Mathf.Max( best, 
							MiniMax(table, depth+1, !isMax) ); 
						table[i, j] = '1'; 
					} 
				} 
			} 
			return best; 
		}
		else
		{ 
			int best = 1000; 
			for (int i = 0; i<3; i++) 
			{ 
				for (int j = 0; j<3; j++) 
				{ 
					if (table[i, j]=='1') 
					{ 
						table[i, j] = GameManager.instance.human; 
						best = Mathf.Min(best, 
							MiniMax(table, depth+1, !isMax)); 
						table[i, j] = '1'; 
					} 
				} 
			} 
			return best; 
		}  
	}

	void findBestMove() 
	{
		char[,] table = GameManager.instance.table;
		int bestVal = -1000; 
		int bestCol = -1;
		int bestRow = -1;

		for (int i = 0; i<3; i++) 
		{
			for (int j = 0; j<3; j++) 
			{ 
				if (table[i, j]=='1') 
				{ 
					table[i, j] = GameManager.instance.aiBot; 
	
					int moveVal = MiniMax(table, 0, false); 
	
					table[i, j] = '1'; 
	
					if (moveVal > bestVal) 
					{ 
						bestRow = i; 
						bestCol = j; 
						bestVal = moveVal;
					} 
				} 
			}
		}
		if(bestCol == -1 || bestRow == -1)
		{
			UIRef.instance.Equall();
		}
		else
		{
			GameManager.instance.bestRow = bestRow;
			GameManager.instance.bestColumn = bestCol;
			GameManager.instance.table[bestRow , bestCol] = GameManager.instance.aiBot;
			UIRef.instance.PutTableInGUI(bestRow, bestCol);
			
			if(GameManager.instance.EndGameConditionAI())
			{
				UIRef.instance.AIWins();
			}
			GameManager.instance.playerTurn = true;
			UIRef.instance.RefreshUI();
		}
		if(!CheckForFreeSpaces())
		{
			UIRef.instance.Equall();
		}
		iterator = false;
	}

	private bool CheckForFreeSpaces()
	{
		int counter = 0;

		for(int i = 0; i < GameManager.instance.n; i++)
		{
			for(int j = 0; j < GameManager.instance.n; j++)
			{
				if(GameManager.instance.table[i, j] == '1')
				{
					counter++;
				}
			}
		}
		if(counter == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}